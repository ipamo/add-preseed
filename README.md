add-preseed
===========

Add a [preseed file](https://wiki.debian.org/DebianInstaller/Preseed) to a Debian installer ISO file.

**Moved to https://gitlab.com/ipamo/debseed**
